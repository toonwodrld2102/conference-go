import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": "Digital map of " + city + " " + state, "per_page": 1}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_lon_lat(city):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct?"
    params = {"q": city, "appid": OPEN_WEATHER_API_KEY, "limit": 1,}
    response = requests.get(geo_url, params=params)
    content = json.loads(response.content)
    lon = content[0]["lon"]
    lat = content[0]["lat"]
    lon_lat_list = [lon, lat]
    return lon_lat_list


def get_weather(city):
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    lists = get_lon_lat(city)
    lon = lists[0]
    lat = lists[1]
    params = {"lon": lon, "lat": lat, "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(weather_url, params=params)
    content = json.loads(response.content)

    try:
        return {"weather": {"temp": content["main"]["temp"], "description": content["weather"][0]["description"]}}
    except(KeyError, IndexError):
        return {"weather":None}
